const { WebHookMessage } = require('./lib/webHookMessage')
/**
 * Expose internal modules
 */
module.exports = {
    WebHookMessage
}