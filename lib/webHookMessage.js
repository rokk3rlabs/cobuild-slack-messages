const axios   = require('axios');

class WebHookMessage { 
    constructor (config) {
        if (!config || !config.url || !config.channel) {
            throw new Error('No url or channel found');
        }
        this.url = config.url;
        this.channel = config.channel;
    }
    
    /**
     * Send web hook message
     * @param {String} title - MeSsage's title
     * @param {String} message  - Message content
     * @param {Array} [fields] - Fields payload
     * @param {String} fields.title - Field's title
     * @param {String} fields.value - Field's value
     * @param {Object} [extra] - Extra information to the message. E.g footer, color
     * @returnSuccess {Object} Slack message
	 * @returnError {Object} Default slack error
     */

    async send (title, message, fields = [], extra = null) {
        let payload = {
            pretext: title,
            text: message,
            channel: this.channel,
            fields
        }
        if (extra) {
            if (extra.constructor.name === 'Object') {
                for (let key in extra) {
                    payload[key] = extra[key]
                }
            }else {
                throw new Error ('Extra must be an object')
            }
        };
        try {
            let response = await axios.post(this.url, payload)
            return response;
        } catch (err) {
            throw new Error (err.response.data)
        }
    }
}

module.exports = {
    WebHookMessage
}